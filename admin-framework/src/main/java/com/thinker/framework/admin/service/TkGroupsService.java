package com.thinker.framework.admin.service;

import com.thinker.framework.admin.entity.TkGroups;
import com.thinker.framework.framework.database.mybatis.ThinkerIService;

public interface TkGroupsService extends ThinkerIService<TkGroups> {
}
