package com.thinker.framework.framework.renders.enums;

public enum FlexAlign {
    TOP, MIDDLE, BOTTOM;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
